# DB

![DB SCHEMA](./dbschema.png)


# GIT COMMANDS

`git pull origin master` downloads latest version from master

`git add <filename>` | `git add *` adds file to staging status

`git commit -m "Commit message"` | `git commit -a` Commit changes to head | Commit any files you've added with git add, and also commit any files you've changed since then:

`git push origin master` Send changes to the master branch

`git fetch origin` Instead, to drop all your local changes and commits, fetch the latest history from the server and point your local master branch at it