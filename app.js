var express = require('express');
var app = express();
const port = 3000;


const mysql = require('mysql2');
var session = require('express-session');
var randomstring = require("randomstring");
const bodyParser = require('body-parser');
const sha256 = require('sha256');


app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(express.static("frontend"));
app.use(express.json());
app.use(bodyParser.urlencoded({
  extended: true,
}));

databasename = "c1restaurant"
const connectionPool = mysql.createPool({
  host: "now.bweb-ssl.de",
  user: "c1restaurantuser",
  password: "restaurant!User",
  database: "c1restaurant",
  port: 3306,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
  multipleStatements: true
});

// give sessionID
app.use(session({
    genid: function(req) {
      let sid = randomstring.generate(36)
      return sid
    },
    secret: randomstring.generate(8)
  }));

  connectionPool.execute(
    'SELECT 1',[],
    function(err, results, fields) {
     if(err){
         console.log(err);
     }
     else{
         console.log("DB Connection succesfully tested");
     }
    }
  );

/* ##################################
            MAIN CODE
#####################################*/



  // returns userID if user is logged in
async function getUserFromSession(sessionID){
    let result = await asyncQuery(`SELECT \`user_id\` FROM \`${databasename}\`.\`session\` WHERE \`ID\`=\'${sessionID}\';`,[]);
  
    if(isNull(result[0])){
      return null
    }else{
      return result[0].user_id
    }
  }

  // returns true if value is Null or something similar
function isNull (value){
    if (value === undefined || value === '' || value === null || value === 'NULL') {
      return true;
    }else{
      return false;
    }
  }

function extractSuccessFromDbResult (result){

   let res;
  try{
    if(result.affectedRows >=1){
      res = {status: 1, message: 'success', databaseOutput: result}
    }else{
      res = {status: -1, message: 'affectedRows = 0   -> nothing changed in database'}
    }
  }catch{
    res = {status: -1, message: 'fail'}
  }

  return res;
}

async function addFoodToRestaurant(RestID, foodtypes){
         sql = ""
         for(i = 0; i<foodtypes.length; i++){
           sql = sql + `INSERT INTO ${databasename}.food (restaurant_id, foodtype_id) VALUES (${RestID}, ${foodtypes[i]}); `
         }
        return await asyncQuery(sql,[])
}

async function addFoodToUser(uid, foodtypes){
    sql = ""
    for(i = 0; i<foodtypes.length; i++){
      sql = sql + `INSERT INTO ${databasename}.favorite (user_id, foodtype_id) VALUES (${uid}, ${foodtypes[i]}); `
    }
   return await asyncQuery(sql,[])
}


async function getPersonalizedRating(userID, restaurantID){
    userID = 24
    try{
        ratings = await asyncQuery(`SELECT * FROM ${databasename}.rating WHERE restaurant_id=?;`,[restaurantID])

        multiplier = 0
        ratingsum = 0
        for(i=0; i<ratings.length; i++){
            weight = 4

            //check rater has a favourite food of Restaurants foodtypes (is Expert in this food)
            commonFood = await asyncQuery(`SELECT favorite.foodtype_id FROM favorite, food WHERE favorite.foodtype_id = food.foodtype_id AND favorite.user_id = ? AND food.restaurant_id = ?`,[ratings[i].user_id,restaurantID])
            if(commonFood.length > 0){
                weight += 2

                //check user has one of these foods as favorite as well
                has = 0
                for(k=0; k<commonFood.length; k++){
                    allcommon = await asyncQuery(`SELECT * FROM favorite WHERE favorite.user_id = ? AND favorite.foodtype_id = ?;`,[userID,commonFood[k].foodtype_id])
                    if(allcommon.length > 0){
                        has = 3
                        break
                    }
                }
                weight += has
            }

            //check rater is from same country as user
            commonCountry = await asyncQuery(`SELECT * FROM user rater, user u WHERE rater.country = u.country AND rater.ID = ? AND u.ID = ?`,[ratings[i].user_id,userID])
            if(commonCountry.length > 0){
                weight += 1
            }

            ratingsum += ratings[i].stars*weight
            multiplier += weight
        }

        return (ratingsum/multiplier)
    }catch(err){
        return err
    }

}


/*
sqlCreateTableRestaurant();
sqlCreateTableUser();
sqlCreateTableFood();
sqlCreateTableFavorite();
sqlCreateTableFoodtype();
sqlCreateTableRating();
*/


/* ##################################
            API FUNCTIONS
#####################################*/

app.get("/api/test", async (req,res)=>{
    console.log(req);
    res.send("hallo");
})


// übergeben:  username, email, password, country, foodtypes = [1, 3, 5, 7]}
app.post("/api/user/register", async (req, res) =>{
    if(isNull(req.body.username)){
      return res.status(400).json({status: -1, error: 'missing username'});
  
    }else if(isNull(req.body.email)){
      return res.status(400).json({status: -1, error: 'missing email'});
  
    }else if(isNull(req.body.password)){
      return res.status(400).json({status: -1, error: 'missing password'});
    }else if(isNull(req.body.country)){
        return res.status(400).json({status: -1, error: 'missing country'});
    }
  
    const salt = randomstring.generate(64);
    const pwHash = sha256(salt+req.body.password);  //hash salt with password
  
    try {
      await asyncQuery(`INSERT INTO ${databasename}.user (name, email, country, pw, salt) VALUES (?,?,?,?,?)`, [req.body.username, req.body.email, req.body.country, pwHash, salt]);
      
      try{
        result = await asyncQuery(`SELECT ID FROM ${databasename}.user WHERE email = ?;`,[req.body.email])

        if(!isNull(result)){
            uid = result[0].ID

            if(!isNull(req.body.foodtypes)){
                return res.send(addFoodToUser(uid,req.body.foodtypes))
            }else{
                return res.status(400).json({status: 1, message: 'user added, no foodtypes provided'});
            }
            
        }
        return res.status(400).json({status: -1, message: 'insert failed'});
        
        }catch(err){
            return res.status(400).json({status: -1, error: err});
        }
            
    
      
     

      
   } catch (err) {
       return res.json({status: -1, message: 'mail or username already in use', error: err});
   }
  })

app.get("/api/checkLoggedIn/:uID", async (req, res) =>{
    //already logged in?
    if(!isNull(await getUserFromSession(req.sessionID))){
      const uid = await getUserFromSession(req.sessionID);
      return res.json({status: 1, message: 'account was logged in', UserID : uid});
    }else
    {
        return res.json({status: -1, error: 'user not logged in'});
    }
});

// login(email,password) : boolean
// uses sessionID, that user already has when entering the page
app.post("/api/user/login", async (req, res) =>{
    //already logged in?
    if(!isNull(await getUserFromSession(req.sessionID))){
      const uid = await getUserFromSession(req.sessionID);
      return res.json({status: 1, message: 'login successful', UserID : uid});
    }
    //email available?
    if(isNull(req.body.email)){
      if(req.body.email = ""){
        return res.status(400).json({status: -1, error: 'missing email'});
      }
  
    // password available?
    }else if(isNull(req.body.password)){
      if(req.body.password = "")
      {
        return res.status(400).json({status: -1, error: 'missing password'});
      }
    }
  
    //delete all old sessions of this user in the database
    //get userID
    let sql = `SELECT ID FROM ${databasename}.user WHERE email= '${req.body.email}';`;
    let result = await asyncQuery(sql,[]);
  
    let userID = 0;
  
    if(!isNull(result[0])){
      userID = result[0].ID;
    }else{
      return res.json({status: -1, message:'user not found', userID : -1})
    }

    // delete old sessions of this user
    result = await asyncQuery(`DELETE FROM ${databasename}.session WHERE user_id=?;`,[userID]);
  
    //get salt and pwhash from database
    result = await asyncQuery(`SELECT salt, pw FROM ${databasename}.user WHERE ID=?;`,[userID]);
    const salt = result[0].salt;
    const pwhash = result[0].pw;
  
    if(sha256(salt+req.body.password) === pwhash){
      // Login successful -> create session      
      const rows =  await asyncQuery(`INSERT INTO ${databasename}.session (ID, user_id) VALUES (?,?)`,[req.sessionID, userID]);    
      return res.json({status: 1, message: 'login successful', UserID : userID});
    }else{
  
      return res.json({status: -1, message: 'email or password is wrong'});
    }  
  })

// logout(): bool
// deletes session
app.post("/api/user/logout", async (req,res) =>{
    //logged in?
    if(isNull(await getUserFromSession(req.sessionID))){
      return res.json({status: -1, error: 'you cannot logout, because you are not logged in'});
    }
  
    try{
    //delete session
    const result = await asyncQuery(`DELETE FROM ${databasename}.session WHERE ID=?;`,[req.sessionID])
    
    return res.json(extractSuccessFromDbResult(result));
   } catch (err) {
       return res.json({status: -1, message: 'fail', error: err});
   }
  
  })


app.get("/api/getAllFoodtypes", async (req,res)=>{
    try{
    result = await asyncQuery("SELECT * FROM foodtype",[]);
    }catch(err){
        return res.status(400).json({status: -1, error: err})
    }

    return res.send(result)
    
})


// übergeben: foodtype in url
app.post("/api/addFoodtype/:foodtype", async(req,res)=>{
    try{
        await asyncQuery(`INSERT INTO ${databasename}.foodtype (name) VALUES (?);`,[req.params.foodtype])
    }catch(err){}

    result = await asyncQuery(`SELECT * FROM ${databasename}.foodtype WHERE name=?;`,[req.params.foodtype])

    return res.send(result)
})

/*
Übergeben:
json = {name = "name", 
        price = 2, 
        about = "about description", 
        city = "City of Restaurant", 
        foodtypes = [1, 3, 5, 7]}
*/
app.post("/api/addRestaurant", async (req, res) =>{

    const parameters = req.body

    name = parameters.name
    price = parameters.price
    about = parameters.about
    city = parameters.city
    foodtypes = parameters.foodtypes

    if(isNull(name)){
      return res.status(400).json({status: -1, error: 'name missing'});
  
    }else if(isNull(price)){
      return res.status(400).json({status: -1, error: 'price missing'});
  
    }else if(isNull(about)){
        return res.status(400).json({status: -1, error: 'missing about'});
    
      }else if(isNull(city)){
        return res.status(400).json({status: -1, error: 'missing city'});
      }

      try {
      result = await asyncQuery(`INSERT INTO ${databasename}.restaurant (name, price, about, city) VALUES (?,?,?,?);`, [name,price,about,city]);
      
      resraurantIDresult = await asyncQuery(`SELECT ID FROM ${databasename}.restaurant WHERE name=? AND price=? AND about=? AND city=?;`, [name,price,about,city]);
      const rID = resraurantIDresult[0].ID


      if(isNull(rID)){
        return res.json({status: -1, message: 'fail RestaurantID is NULL'});
      }else{
        // insert into food table
        if(!isNull(foodtypes)){
            result = await addFoodToRestaurant(rID, foodtypes)
            return res.send(result)
        }
        return res.json({status: 1, message: `Restaurant ${rID} added`});
      }

   } catch (err) {
       return res.json({status: -1, message: 'fail', error: err});
   }
  })

app.get("/api/addFoodtypeToRestaurant/:RestaurantID/:FoodtypeID", async(req,res)=>{
    rid = req.params.RestaurantID
    fid = req.params.FoodtypeID

    try{
        result = await asyncQuery(`INSERT INTO ${databasename}.food (restaurant_id, foodtype_id) VALUES (?,?)`,[rid,fid]);
        return res.send(extractSuccessFromDbResult(result))
    }catch(err){
        return res.send(err)
    }
})

//Foodtype Preference to logged user
app.get("/api/addFoodtypePrefToUser/:FoodtypeID", async(req,res)=>{
    uid = await getUserFromSession(req.sessionID)
    fid = req.params.FoodtypeID

    if(isNull(uid)){
        return res.json({status : -1, message : 'not logged in'})
      }
      if(isNull(fid)){
        return res.json({status : -1, message : 'FoodtypeID not provided'})
      }

    try{
        result = await asyncQuery(`INSERT INTO ${databasename}.favorite (user_id, foodtype_id) VALUES (?,?)`,[uid,fid]);
        return res.send(extractSuccessFromDbResult(result))
    }catch(err){
        return res.send(err)
    }
})

// optional: name, city, foodtype(key) -> if no parameter is given all restaurants will be returned 
app.get("/api/getRestaurant",async (req, res)=>{
    name = req.query.name
    city = req.query.city
    foodtype = req.query.foodtype

    sql = `SELECT restaurant.ID, name, price, about, city FROM ${databasename}.restaurant`

    if(!isNull(foodtype)){
        sql = sql+`, ${databasename}.food WHERE food.restaurant_id = restaurant.id AND foodtype_id = ${foodtype} AND `
    }else{
        sql = sql+" WHERE "
    }

    if(!isNull(name)){
        sql = sql+` name = '${name}' AND `
    }

    if(!isNull(city)){
        sql = sql+` city = '${city}' AND `
    }

    if(sql.substring(sql.length-4,sql.length) == "AND "){
        sql = sql.substring(0,sql.length-4)+";"

    }else if(sql.substring(sql.length-6,sql.length) == "WHERE "){
        sql = sql.substring(0,sql.length-6)+";"

    }

    try{
        result = await asyncQuery(sql,[])
        return res.send(result)
    }catch(err){
        return res.send(err)
    }


})

app.get("/api/getRestaurant/:RestaurantID", async (req,res)=>{
    try{
        result = await asyncQuery(`SELECT * FROM ${databasename}.restaurant WHERE ID=?;`,[req.params.RestaurantID])
        return res.send(result)
    }catch(err){
        return res.send(err)
    }

})

app.get("/api/getFoodtypesOfRestaurant/:RestaurantID",async(req,res)=>{
    rid = req.params.RestaurantID
    try{
        result = await asyncQuery(`SELECT foodtype.ID, name FROM ${databasename}.foodtype, ${databasename}.food WHERE foodtype.ID = foodtype_id AND restaurant_id = ?;`,[rid])
        return res.send(result)
    }catch(err){
        return res.send(err)
    }
    
})

app.get("/api/getAllRatingsForRestaurant/:RestaurantID", async(req,res)=>{
    rid = req.params.RestaurantID

    if(isNull(rid)){
        return res.json({status : -1, message : 'RestaurantID not provided'})
      }

    try{
        result = await asyncQuery(`SELECT * FROM ${databasename}.rating where restaurant_id = ?`,[rid])
        return res.send(result)
    }catch(err){
        return res.send(err)
    }
})

app.get("/api/getRatingsAvgRestaurant/:RestaurantID", async(req,res)=>{
    rid = req.params.RestaurantID

    if(isNull(rid)){
        return res.json({status : -1, message : 'RestaurantID not provided'})
      }

    try{
        result = await asyncQuery(`SELECT AVG(stars) AS Rating FROM ${databasename}.rating where restaurant_id = ?;`,[rid])
        return res.send(result)
    }catch(err){
        return res.send(err)
    }
})

app.get("/api/getUser/:UserID", async(req,res)=>{
    uid = req.params.UserID

    if(isNull(uid)){
        return res.json({status : -1, message : 'UserID not provided'})
      }

    try{
        result = await asyncQuery(`SELECT * FROM ${databasename}.user where ID = ?`,[uid])
        return res.send(result)
    }catch(err){
        return res.send(err)
    }

    

})

app.get("/api/getUserPreferences/:UserID", async(req,res)=>{
    uid = req.params.UserID

    if(isNull(uid)){
        return res.json({status : -1, message : 'UserID not provided'})
      }

    try{
        result = await asyncQuery(`SELECT * FROM ${databasename}.favorite, ${databasename}.foodtype WHERE foodtype.ID = favorite.foodtype_id AND user_id = ?`,[uid])
        return res.send(result)
    }catch(err){
        return res.send(err)
    }

    

})

//user must be logged in
//parameter: RestaurantID, stars, text(optional)
app.post("/api/addRating", async(req,res)=>{
    uid = await getUserFromSession(req.sessionID)
    rid = req.body.RestaurantID
    stars = req.body.stars
    text = req.body.text

    if(isNull(uid)){
        return res.json({status : -1, message : 'not logged in'})
      }

    if(isNull(rid)){
      return res.json({status : -1, message : 'RestaurantID not provided'})
    }

    if(isNull(stars)){
        return res.json({status : -1, message : 'stars not provided'})
      }

    try{
        result = await asyncQuery(`INSERT INTO ${databasename}.rating (user_id, restaurant_id, stars, text) VALUES (?,?,?,?);`,[uid,rid,stars,text])

        return res.send(extractSuccessFromDbResult(result))
    }catch(err){
        return res.send(err)
    }
})

//user must be logged in
app.get("/api/getPersonalizedRating/:RestaurantID",async(req,res)=>{
    uid = await getUserFromSession(req.sessionID)
    rid = req.params.RestaurantID

    if(isNull(uid)){
       //return res.json({status : -1, message : 'not logged in'})
      }
    if(isNull(rid)){
      return res.json({status : -1, message : 'RestaurantID not provided'})
    }

    return res.json({status : 1, RestaurantID: rid, rating : await getPersonalizedRating(uid,rid), YourUserID: uid})
})

//user must be logged in
//parameter: city (optional)
app.get("/api/getRecommendation",async(req,res)=>{
    uid = await getUserFromSession(req.sessionID)
    city = req.body.city

    if(isNull(uid)){
      // return res.json({status : -1, message : 'not logged in'})
      }
    
    sql = "SELECT ID FROM restaurant "

    if(!isNull(city)){
        sql = sql +"WHERE city = "+city
    }

    restaurants = await asyncQuery(sql,[])
    getting = []
    for(n=0; n<restaurants.length; n++){
        getting.push({restaurantID: restaurants[n].ID, rating: await getPersonalizedRating(uid,restaurants[n].ID)})
        
    }
    return res.send(getting)
})




/* ##################################
        CREATE TABLE FUNCTIONS
#####################################*/


async function sqlCreateTableRestaurant()
{
    try{
        res = await asyncQuery("CREATE TABLE restaurant ( ID int NOT NULL AUTO_INCREMENT PRIMARY KEY, name VARCHAR(64), price INT, open INT, about VARCHAR(65535), country VARCHAR(64),  city VARCHAR(64), street VARCHAR(64), Housenumber INT)",[]);
    }catch(err){
    }
}

async function sqlCreateTableUser()
{
    try{
        res = await asyncQuery("CREATE TABLE user ( ID int NOT NULL AUTO_INCREMENT PRIMARY KEY, name VARCHAR(64), email VARCHAR(64), regdate DATETIME DEFAULT CURRENT_TIMESTAMP, country VARCHAR(64), pw VARCHAR(512), salt VARCHAR(128) )",[]);
        console.log(res);
    }catch(err){
        console.log(err);
    }
}

async function sqlCreateTableRating()
{
    try{
        res = await asyncQuery("CREATE TABLE rating ( ID int NOT NULL AUTO_INCREMENT PRIMARY KEY, user_id INT, food_id INT, stars INT, text VARCHAR(512), FOREIGN KEY (user_id) REFERENCES user(ID), FOREIGN KEY (food_id) REFERENCES food(ID)) ",[]);
        console.log(res);
    }catch(err){
        console.log(err);
    }
}

async function sqlCreateTableFood()
{
    try{
        res = await asyncQuery("CREATE TABLE food ( ID int NOT NULL AUTO_INCREMENT PRIMARY KEY, restaurant_id INT, foodtype_id INT, FOREIGN KEY (restaurant_id) REFERENCES restaurant(ID), FOREIGN KEY (foodtype_id) REFERENCES foodtype(ID)) ",[]);
        console.log(res);
    }catch(err){
        console.log(err);
    }
}

async function sqlCreateTableFoodtype()
{
    try{
        res = await asyncQuery("CREATE TABLE foodtype ( ID int NOT NULL AUTO_INCREMENT PRIMARY KEY, name VARCHAR(64)) ",[]);
        console.log(res);
    }catch(err){
        console.log(err);
    }
}

async function sqlCreateTableFavorite()
{
    try{
        res = await asyncQuery("CREATE TABLE favorite ( ID int NOT NULL AUTO_INCREMENT PRIMARY KEY, user_id INT, foodtype_id INT, FOREIGN KEY (user_id) REFERENCES user(ID), FOREIGN KEY (foodtype_id) REFERENCES foodtype(ID)) ",[]);
        console.log(res);
    }catch(err){
        console.log(err);
    }
}


 /* GLOBAL SQL QUERY STATEMENT */
async function asyncQuery(sql, args) {
    return new Promise((resolve, reject) => {
        connectionPool.getConnection(function(err, connection) {
            if (err) {
                if (connection !== undefined) {
                    connection.release();
                }
                return reject(err);
            }
            connection.query(sql, args, (err, rows) => {
                    //console.log("SQL Query: "+mysql.format(sql, args));
                connection.release();
                if (err) {
                    return reject(err);
                }
                resolve(rows);
            });
        });
    });
  }

app.listen(port, function () {
    console.log('App listening on port 3000!');
});


// -----------------------  Tests

app.get("/api/user/register", (req,res)=>{
    res.send(`
      <h1>Registrieren Test<h1>
      <form method='post' action='/api/user/register'>
        <input name='username' placeholder='username' required />
        <input type='email' name='email' placeholder='email' required />
        <input type='password' name='password' placeholder='password' required />
        <input name='country' placeholder='country' required />
        <input type='submit' />
      </form>
      <a>
    `)
  })

  app.get("/api/addFoodtype", async (req,res)=>{
    res.send(`
      <form method='post' action='/api/addFoodtype'>
      <input name='foodtype' placeholder='foodtype'  />
      <input type='submit' />
      </form>
      <a>
    `)
  })

  app.get("/api/user/login", (req,res)=>{
    res.send(`
      <h1>Login Test<h1>
      <form method='post' action='/api/user/login'>
        <input type='email' name='email' placeholder='email' required />
        <input type='password' name='password' placeholder='password' required />
        <input type='submit' />
      </form>
      <a>
    `)
  })

  app.get("/api/user/logout", (req,res)=>{
    res.send(`
      <h1>Logout Test<h1>
      <form method='post' action='/api/user/logout'>
        <input type='submit' />
      </form>
      <a>
    `)
  })

  
  app.get("/api/getR", async (req,res)=>{
    res.send(`
      <form method='get' action='/api/getRestaurant'>
      <input name='foodtype' placeholder='foodtype'  />
      <input name='name' placeholder='name'  />
      <input name='city' placeholder='city'  />
      <input type='submit' />
      </form>
      <a>
    `)
  })

  
  app.get("/api/addRestaurant", (req,res)=>{
    res.send(`
      <h1>add Restaurant<h1>
      <form method='post' action='/api/addRating'>
        <input name='RestaurantID' placeholder='RestaurantID' required />
        <input name='stars' placeholder='stars' required />
        <input name='text' placeholder='text'/>
        <input type='submit' />
      </form>
      <a>
    `)
  })


  app.get("/api/addRating", (req,res)=>{
    res.send(`
      <h1>add Rating<h1>
      <form method='post' action='/api/addRating'>
        <input name='RestaurantID' placeholder='RestaurantID' required />
        <input name='stars' placeholder='stars' required />
        <input name='text' placeholder='text'/>
        <input type='submit' />
      </form>
      <a>
    `)
  })
