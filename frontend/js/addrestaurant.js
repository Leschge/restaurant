$( document ).ready(function() {

    DOMAIN = "";

    $(".form-add").bind( "submit", function(e) {
        e.preventDefault();
        
        form = $(this).serializeArray();
        console.log(form);
        /*
        json = {name = "name", 
        price = 2, 
        about = "about description", 
        city = "City of Restaurant", 
        foodtypes = [1, 3, 5, 7]}
*/

        formName        = form[0]["value"];
        formCity       = form[1]["value"];
        formAbout       = form[2]["value"];
        formPrice        = form[3]["value"];
        formFoodtypes = [];

        for(i = 0 ; i < form.length ; i++)
        {
            if(form[i]["name"] == "formFoodtypes")
            {
                formFoodtypes.push(parseInt(form[i]["value"]))
            }
        }

        var eventData = {
            "name": formName,
            "price": formPrice,
            "about": formAbout,
            "city": formCity,
            "foodtypes" : formFoodtypes
            };

            console.log(eventData);
         
            URL = DOMAIN+"/api/addRestaurant";
            var addRestaurant = $.ajax({
				url : URL,
				method: "POST",
				data: eventData,
				dataType: "json"
				}
            );
            
            addRestaurant.done(function( done ) {
                console.log(done);
                if(done.status == -1){
                    $(".alert").html(done.message);
                    $(".alert").show();
                }
                else{
                    alert("Restaurant added!");
                    window.location.href = 'index.html';
                }
            });

            addRestaurant.fail(function( fail ) {
                console.log(fail);
                res = JSON.parse(fail.responseText);
                $(".alert").html(res.error);
                $(".alert").show();
            });


    
    });






    URL = DOMAIN+"/api/getAllFoodtypes";
    var getFoodtypes = $.ajax({
		url : URL,
		method: "GET"
		}
    );
    
    getFoodtypes.done(function( done ) {
        $("#loadingFoodtypes").fadeOut();
        done.forEach(
            type => 
            {
                var parent = document.getElementById("foodtypes");
                var child = document.createElement('option');
                child.text = type.name;
                child.value = type.ID;
                parent.add(child);
            });
    })

    getFoodtypes.fail(function( fail ) {
        res = JSON.parse(fail.responseText);
        $(".alert").html(res.error);
        $(".alert").show();
    });

});