$( document ).ready(function() {

    USERID = null;
    DOMAIN = "";

    /* ##### CHECK LOGIN ##### */

    if(localStorage.getItem('userID'))
    {
        USERID = localStorage.getItem('userID');

        URL = DOMAIN+"/api/checkLoggedIn/"+USERID;
        var isLoggedIn = $.ajax({
            url : URL,
            method: "GET"
        });

        isLoggedIn.done(function( done ) {
            if(done.status == 1)
            {
                URL = DOMAIN+"/api/getUserPreferences/"+USERID;
                var getPreferences = $.ajax({
                    url : URL,
                    method: "GET"
                });

                getPreferences.done(function( done ) {
                    $("#user-buttons").fadeIn();

                    done.forEach(
                    type => 
                    {
                         $("#nav-pref").append("<div class='foodtype'><i>"+type.name+"</i></div>");
                         $("#nav").hide();
                         $("#nav-pref").show();
                    });
                });

                getPreferences.fail(function( fail ) {
                    console.log(done)
                });
            }
            else
            {
                USERID = null;
                localStorage.removeItem('userID');
            }
        });

        isLoggedIn.fail(function( fail ) {
            USERID = null;
            localStorage.removeItem('userID');
        });
    }

    /* ##### LOGOUT ##### */
    
    $("#btn-logout").click(function()
    {
        URL = DOMAIN+"/api/user/logout";
        var logout = $.ajax({
		url : URL,
		method: "POST"
        });
        
        logout.done(function( done ) {
            if(done.status == 1)
            {
                window.location.href = 'index.html';
            }
        });
    
    });
    
    /* #####    SEARCHBAR   ##### */

    URL = DOMAIN+"/api/getAllFoodtypes";
    var getFoodtypes = $.ajax({
		url : URL,
		method: "GET"
		}
    );
    
    getFoodtypes.done(function( done ) {
        $("#loadingFoodtypes").fadeOut();
        done.forEach(
            type => 
            {
                var parent = document.getElementById("selectFoodtype");
                var child = document.createElement('option');
                child.text = type.name;
                child.value = type.ID;
                parent.add(child);
            });
    })




    $(".form-search").bind( "submit", function(e) {
        e.preventDefault();
        
        form = $(this).serializeArray();


        searchName      = form[0]["value"];
        searchCity      = form[1]["value"];
        searchFoodtype  = "";
        if(form[2]["value"] != "")
        {
            searchFoodtype = parseInt(form[2]["value"]);
        }

        var eventData = {
            "name": searchName,
            "city": searchCity,
            "foodtype": searchFoodtype
            };

        URL = DOMAIN+"/api/getRestaurant";
        var getRestaurantSearch = $.ajax({
            url : URL,
            method: "GET",
            data: eventData,
			dataType: "json"
            }
        );
        
        getRestaurantSearch.done(function( done ) {

            if(USERID != null) 
            {
                 //getPersonalRatings(rest.ID);
                loadRecommendation()
            }

            $(".restaurant").remove();
            $(".rating-container").remove();

            prices = [ "Super Cheap" , "Cheap" , "Normal" , "Expensive"];

            done.forEach(
                rest => 
                {

                    URL = DOMAIN+"/api/getFoodtypesOfRestaurant/"+rest.ID;
                    var getRestaurantFoodtypes = $.ajax({
                        url : URL,
                        method: "GET"
                    });

                    getRestaurantFoodtypes.done(function( done ) {
                        done.forEach(
                            type => 
                            {
                                $("#foodtypes"+rest.ID).append("<div class='foodtype'><i>"+type.name+"</i></div>");
                            })
                    });


                    URL = DOMAIN+"/api/getRatingsAvgRestaurant/"+rest.ID;
                    var getRestaurantStars = $.ajax({
                        url : URL,
                        method: "GET"
                    });

                    getRestaurantStars.done(function( done ) {
                        stars = "-"
                        if(done[0].Rating != null) { stars = done[0].Rating }
                        $("#stars"+rest.ID).append(stars);
                    });

                    listRestaurants(rest);
  
                }); 
        })

        getRestaurantSearch.fail(function( fail ) {
            console.fail(fail);
        })
    });


    /* ##### GET ALL RESTAURANTS ##### */

    URL = DOMAIN+"/api/getRestaurant";
    var getRestaurant = $.ajax({
		url : URL,
		method: "GET"
		}
    );
    
    getRestaurant.done(function( done ) {

        if(USERID != null) 
        {
            //getPersonalRatings(rest.ID);
            loadRecommendation()
        }


        $("#loadingRestaurants").remove();

        prices = [ "Super Cheap" , "Cheap" , "Normal" , "Expensive"];

        done.forEach(
            rest => 
            {

                URL = DOMAIN+"/api/getFoodtypesOfRestaurant/"+rest.ID;
                var getRestaurantFoodtypes = $.ajax({
                    url : URL,
                    method: "GET"
                });

                getRestaurantFoodtypes.done(function( done ) {
                    done.forEach(
                        type => 
                        {
                            $("#foodtypes"+rest.ID).append("<div class='foodtype'><i>"+type.name+"</i></div>");
                        })
                });


                URL = DOMAIN+"/api/getRatingsAvgRestaurant/"+rest.ID;
                var getRestaurantStars = $.ajax({
                    url : URL,
                    method: "GET"
                });

                getRestaurantStars.done(function( done ) {
                    stars = "-"
                    if(done[0].Rating != null) { stars = done[0].Rating }
                    $("#stars"+rest.ID).append(stars);
                });

                listRestaurants(rest);
  
            }); 
    })

    getRestaurant.fail(function( fail ) {
        res = JSON.parse(fail.responseText);
        $(".alert").html(res.error);
        $(".alert").show();
    });

    
    /* ##### GET RATINGS FOR RESTAURANT ##### */

    $("#rateRestClose").click(function(){
        $("#rateRest-container").fadeOut();
    })


    $("body").delegate(".rat-btn", "click", function(){
        var restID = $(this).attr('value');

        $("#rateRest-container").fadeIn();
        $("#ratRestID").val(restID);
    });


    $(".form-addRating").bind( "submit", function(e) {
        e.preventDefault();
        
        form = $(this).serializeArray();

        ratStars    = form[0]["value"];
        ratText     = form[1]["value"];
        ratRestID   = form[2]["value"];

        var eventData = {
            "RestaurantID": parseInt(ratRestID),
            "stars": parseInt(ratStars),
            "text": ratText
            };

        URL = DOMAIN+"/api/addRating";
        var addRating = $.ajax({
			url : URL,
			method: "POST",
			data: eventData,
			dataType: "json"
		});
            
            
        addRating.done(function( done ) {
            if(done.status == 1)
            {
                $("#rateRest-container").fadeOut();
            }
            else{
                alert(done.message);
            }
        });

        addRating.fail(function( fail ) {
            console.log(done)
            res = JSON.parse(fail.responseText);
            $(".alert").html(res.error);
            $(".alert").show();
        });
    
    });





    $("body").delegate(".rev-btn", "click", function(){
        var restID = $(this).attr('value');
        $(this).fadeOut();
        
        URL = DOMAIN+"/api/getAllRatingsForRestaurant/"+restID;
        var getRestRev= $.ajax({
            url : URL,
            method: "GET"
        })
        getRestRev.done(function( done ) {
            done.forEach(
            rev => 
            {
                $("#"+rev.restaurant_id).append("<div class='review'>"+rev.stars+" - "+rev.text+"</div>");
            });
        })
        getRestRev.fail(function( fail ) {
            console.log(done)
        });

    });


    /* ##### GET PERSONAL RATINGS ##### */
    
    function getPersonalRatings(restID){
        console.log("GET PERSONAL RATING FOR: "+restID)
        URL = DOMAIN+"/api/getPersonalizedRating/"+restID;
        var getPersRatings = $.ajax({
            url : URL,
            method: "GET"
            }
        );
        
        getPersRatings.done(function( done ) {
            console.log(done)

            if(done.status == 1)
            {
                if(done.rating != null)
                {
                    if(typeof done.rating == "number")
                    {
                        $("#starsPers"+done.RestaurantID).html("✨ "+done.rating);
                    }
                    else
                    {
                        $("#starsPers"+done.RestaurantID).html("✨ -");
                    }
                }
                else
                {
                    $("#starsPers"+done.RestaurantID).html("✨ -");
                }
            }
        });

    }


    /* ##### GET RECOMMENDATIONS ##### */

    function loadRecommendation()
    {
        form = $("#searchForm").serializeArray();

        searchCity      = form[1]["value"];

        var eventData = {
            "city": searchCity
        };

        URL = DOMAIN+"/api/getRecommendation";
        var recom = $.ajax({
            url : URL,
            method: "GET"
            }
        );
        
        recom.done(function( done ) {

            $("#loadingRecommendation").fadeOut();

            summ = 0;
            num = 0;

            done.forEach(
                rec =>{
                    num += 1;
                    if(rec.rating != null)
                    {
                        if(typeof rec.rating == "number") 
                        {
                            summ += rec.rating
                            $("#starsPers"+rec.restaurantID).html("✨ "+(""+rec.rating).substring(0,4));
                        }
                    }
            })

            avg = summ/num

            done.forEach(
                rec =>{
                    if(rec.rating != null)
                    {
                        if(typeof rec.rating == "number") 
                        {
                            if(rec.rating > avg)
                            {
                                $("#"+rec.restaurantID).addClass("recommended");
                            }
                        }
                    }
            })

            });
    
        recom.fail(function( fail ) {
            console.log(fail)
        });
    };


    function listRestaurants(restaurantObj){
        rest = restaurantObj;

        $("#restaurant-container").append(
            "<div class='restaurant' id='"+rest.ID+"'>\
            <h5><i class='restaurant-id'>"+rest.ID+" - </i>"+rest.name+"</h5>📌 \
            "+rest.city+" &nbsp; &nbsp; &nbsp; 💲 "+prices[rest.price]+" &nbsp; &nbsp; &nbsp; <span id='stars"+rest.ID+"'>⭐ </span> &nbsp; &nbsp; &nbsp; <span id='starsPers"+rest.ID+"'> </span><br>"+rest.about+"\
            </div>");

        $("#"+rest.ID).append("<div id='foodtypes"+rest.ID+"' class='foodtypes-container'></div>");
        $("#restaurant-container").append(
        "<div class='rating-container'>\
            <div value='"+rest.ID+"' class='rev-btn'>Show Reviews</div>\
            <div value='"+rest.ID+"' class='rat-btn'>Rate Restaurant</div>\
        </div>");
    }

    
 
});