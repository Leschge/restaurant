$( document ).ready(function() {

    DOMAIN = "";

    $(".form-login").bind( "submit", function(e) {
        e.preventDefault();
        
        form = $(this).serializeArray();
        console.log(form);

        logEmail    = form[0]["value"];
        logPw       = form[1]["value"];

        var eventData = {
            "email": logEmail,
            "password": logPw
            };


        URL = DOMAIN+"/api/user/login";
        var login = $.ajax({
			url : URL,
			method: "POST",
			data: eventData,
			dataType: "json"
		});
            
            
        login.done(function( done ) {
            console.log(done)
            if(done.status == -1){
                $(".alert").html(done.message);
                $(".alert").show();
            }
            else{
                localStorage.setItem('userID',done.UserID);
                window.location.href = 'index.html';
            }
        });

        login.fail(function( fail ) {
            res = JSON.parse(fail.responseText);
            $(".alert").html(res.error);
            $(".alert").show();
        });
    
    });
});