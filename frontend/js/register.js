$( document ).ready(function() {

    DOMAIN = "";

    $(".form-reg").bind( "submit", function(e) {
        e.preventDefault();
        
        form = $(this).serializeArray();

        regUsername     = form[0]["value"];
        regEmail        = form[1]["value"];
        regCountry      = form[2]["value"];
        regPw           = form[3]["value"];
        regPwConfirm    = form[4]["value"];
        regFoodtypes = [];

        for(i = 0 ; i < form.length ; i++)
        {
            if(form[i]["name"] == "regFoodtypes")
            {
                regFoodtypes.push(parseInt(form[i]["value"]))
            }
        }


        var eventData = {
            "username": regUsername,
            "email": regEmail,
            "country": regCountry,
            "password": regPw,
            "foodtypes": regFoodtypes
            };

        if(regPw == regPwConfirm)
        {
         
            URL = DOMAIN+"/api/user/register";
            var register = $.ajax({
				url : URL,
				method: "POST",
				data: eventData,
				dataType: "json"
				}
            );
            
            register.done(function( done ) {
                if(done.status == -1){
                    $(".alert").html(done.message);
                    $(".alert").show();
                }
                else{
                    alert("Successfully registered!");
                    window.location.href = 'login.html';
                }
            });

            register.fail(function( fail ) {
                res = JSON.parse(fail.responseText);
                $(".alert").html(res.error);
                $(".alert").show();
            });

        }else
        {
            $(".alert").html("Passwords not similar");
            $(".alert").show();
        }
    
    });


    URL = DOMAIN+"/api/getAllFoodtypes";
    var getFoodtypes = $.ajax({
		url : URL,
		method: "GET"
		}
    );
    
    getFoodtypes.done(function( done ) {
        $("#loadingFoodtypes").fadeOut();
        done.forEach(
            type => 
            {
                var parent = document.getElementById("foodtypes");
                var child = document.createElement('option');
                child.text = type.name;
                child.value = type.ID;
                parent.add(child);
            });
    })

    getFoodtypes.fail(function( fail ) {
        res = JSON.parse(fail.responseText);
        $(".alert").html(res.error);
        $(".alert").show();
    });


});